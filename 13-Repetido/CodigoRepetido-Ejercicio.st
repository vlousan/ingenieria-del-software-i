!classDefinition: #CantSuspend category: 'CodigoRepetido-Ejercicio'!
Error subclass: #CantSuspend
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #NotFound category: 'CodigoRepetido-Ejercicio'!
Error subclass: #NotFound
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #CustomerBookTest category: 'CodigoRepetido-Ejercicio'!
TestCase subclass: #CustomerBookTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBookTest methodsFor: 'auxiliaries' stamp: 'LM 4/14/2021 18:24:37'!
checkThat: aCustomerBook includesOnly: aCustomerName

	self assert: aCustomerBook numberOfCustomers = 1.
	self assert: (aCustomerBook includesCustomerNamed: aCustomerName)
! !

!CustomerBookTest methodsFor: 'auxiliaries' stamp: 'LM 4/14/2021 18:01:45'!
expectFrom: aCustomerBook aNumberOfActive: expectedActive aNumberOfSuspended: expectedSuspended aNumberOfTotal: expectedTotal
	
	self assert: expectedActive equals: aCustomerBook numberOfActiveCustomers.
	self assert: expectedSuspended equals: aCustomerBook numberOfSuspendedCustomers.
	self assert: expectedTotal equals: aCustomerBook numberOfCustomers.! !

!CustomerBookTest methodsFor: 'auxiliaries' stamp: 'LM 4/14/2021 17:23:45'!
takes: anOperation lessMillisecondsThan: aTimeLimitInMilliseconds

	| millisecondsBeforeRunning millisecondsAfterRunning |
	
	millisecondsBeforeRunning := Time millisecondClockValue * millisecond.
	anOperation.
	millisecondsAfterRunning := Time millisecondClockValue * millisecond.
	
	^(millisecondsAfterRunning-millisecondsBeforeRunning) < (aTimeLimitInMilliseconds * millisecond)
	
! !


!CustomerBookTest methodsFor: 'testing' stamp: 'LM 4/14/2021 17:23:45'!
test01AddingCustomerShouldNotTakeMoreThan50Milliseconds

	| customerBook |
	
	customerBook := CustomerBook new.
	
	self assert: (self takes: [customerBook addCustomerNamed: 'John Lennon'.] lessMillisecondsThan: 50).
	! !

!CustomerBookTest methodsFor: 'testing' stamp: 'LM 4/14/2021 20:13:20'!
test02RemovingCustomerShouldNotTakeMoreThan100Milliseconds

	| customerBook paulMcCartney |
	
	customerBook := CustomerBook new.
	paulMcCartney := 'Paul McCartney'.
	customerBook addCustomerNamed: 'John Lennon'.
	
	self assert: (self takes: [	customerBook removeCustomerNamed: paulMcCartney.] lessMillisecondsThan: 100).
	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'LM 4/14/2021 20:44:57'!
test03CanNotAddACustomerWithEmptyName 

	| customerBook |
			
	customerBook := CustomerBook new.

	[ customerBook addCustomerNamed: ''.
	self fail ]
		on: Error 
		do: [ :anError | 
			self assert: anError messageText = CustomerBook customerCanNotBeEmptyErrorMessage.
			self assert: customerBook isEmpty ]! !

!CustomerBookTest methodsFor: 'testing' stamp: 'LM 4/14/2021 20:13:47'!
test04CanNotRemoveAnInvalidCustomer
	
	| customerBook johnLennon |
	
	customerBook := CustomerBook new.
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.
	
	[ customerBook removeCustomerNamed: 'Paul McCartney'.
	self fail ]
		on: NotFound 
		do: [ :anError | 
			self checkThat: customerBook includesOnly: johnLennon ]
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'LM 4/14/2021 20:14:29'!
test05SuspendingACustomerShouldNotRemoveItFromCustomerBook

	| customerBook paulMcCartney|
	
	customerBook := CustomerBook new.
	paulMcCartney := 'Paul McCartney'.
	
	customerBook addCustomerNamed: paulMcCartney.
	customerBook suspendCustomerNamed: paulMcCartney.
	
	self expectFrom: customerBook aNumberOfActive: 0 aNumberOfSuspended: 	1 aNumberOfTotal: 1.
	self assert: (customerBook includesCustomerNamed: paulMcCartney).
	

	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'LM 4/14/2021 20:14:48'!
test06RemovingASuspendedCustomerShouldRemoveItFromCustomerBook

	| customerBook paulMcCartney|
	
	customerBook := CustomerBook new.
	paulMcCartney := 'Paul McCartney'.
	
	customerBook addCustomerNamed: paulMcCartney.
	customerBook suspendCustomerNamed: paulMcCartney.
	customerBook removeCustomerNamed: paulMcCartney.
	
	self expectFrom: customerBook aNumberOfActive: 0 aNumberOfSuspended: 	0 aNumberOfTotal: 0.
	self deny: (customerBook includesCustomerNamed: paulMcCartney).


	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'LM 4/14/2021 20:15:11'!
test07CanNotSuspendAnInvalidCustomer
	
	| customerBook johnLennon |
	
	customerBook := CustomerBook new.
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.
	
	[ customerBook suspendCustomerNamed: 'George Harrison'.
	self fail ]
		on: CantSuspend 
		do: [ :anError | 
			self checkThat: customerBook includesOnly: johnLennon ]
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'LM 4/14/2021 20:15:35'!
test08CanNotSuspendAnAlreadySuspendedCustomer
	
	| customerBook johnLennon |
	
	customerBook := CustomerBook new.
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.
	customerBook suspendCustomerNamed: johnLennon.
	
	[ customerBook suspendCustomerNamed: johnLennon.
	self fail ]
		on: CantSuspend 
		do: [ :anError | 
			self checkThat: customerBook includesOnly: johnLennon ]
! !


!classDefinition: #CustomerBook category: 'CodigoRepetido-Ejercicio'!
Object subclass: #CustomerBook
	instanceVariableNames: 'suspended active'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBook methodsFor: 'requiring' stamp: 'LM 4/14/2021 19:40:23'!
signalExceptionIfAlreadyExists: aName

	(self includesCustomerNamed: aName) ifTrue: [ self signalCustomerAlreadyExists ].! !

!CustomerBook methodsFor: 'requiring' stamp: 'LM 4/14/2021 19:40:11'!
signalExceptionIfCannotBeSuspended: aName

	(active includes: aName) ifFalse: [^CantSuspend signal].! !

!CustomerBook methodsFor: 'requiring' stamp: 'LM 4/14/2021 19:39:52'!
signalExceptionIfNameIsEmpty: aName

	aName isEmpty ifTrue: [ self signalCustomerNameCannotBeEmpty ].! !


!CustomerBook methodsFor: 'testing' stamp: 'NR 4/3/2019 10:14:26'!
includesCustomerNamed: aName

	^(active includes: aName) or: [ suspended includes: aName ]! !

!CustomerBook methodsFor: 'testing' stamp: 'NR 4/3/2019 10:14:26'!
isEmpty
	
	^active isEmpty and: [ suspended isEmpty ]! !


!CustomerBook methodsFor: 'initialization' stamp: 'NR 9/17/2020 07:23:04'!
initialize

	active := OrderedCollection new.
	suspended:= OrderedCollection new.! !


!CustomerBook methodsFor: 'customer management' stamp: 'LM 4/14/2021 19:40:24'!
addCustomerNamed: aName

	self signalExceptionIfNameIsEmpty: aName.
	self signalExceptionIfAlreadyExists: aName.
	
	active add: aName ! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
numberOfActiveCustomers
	
	^active size! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
numberOfCustomers
	
	^active size + suspended size! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 9/19/2018 17:36:09'!
numberOfSuspendedCustomers
	
	^suspended size! !

!CustomerBook methodsFor: 'customer management' stamp: 'LM 4/15/2021 18:18:57'!
removeCustomerNamed: aName 
 
	^active remove: aName ifAbsent: [
		suspended remove: aName ifAbsent: [NotFound signal	].
	]
! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:52'!
signalCustomerAlreadyExists 

	self error: self class customerAlreadyExistsErrorMessage! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:51'!
signalCustomerNameCannotBeEmpty 

	self error: self class customerCanNotBeEmptyErrorMessage ! !

!CustomerBook methodsFor: 'customer management' stamp: 'LM 4/14/2021 19:40:11'!
suspendCustomerNamed: aName 
	
	self signalExceptionIfCannotBeSuspended: aName.
	
	active remove: aName.
	
	suspended add: aName
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerBook class' category: 'CodigoRepetido-Ejercicio'!
CustomerBook class
	instanceVariableNames: ''!

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 4/12/2021 16:39:13'!
customerAlreadyExistsErrorMessage

	^'customer already exists!!!!!!'! !

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 4/12/2021 16:39:09'!
customerCanNotBeEmptyErrorMessage

	^'customer name cannot be empty!!!!!!'! !
