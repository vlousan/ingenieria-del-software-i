!classDefinition: #OOStackTest category: 'Stack-Exercise'!
TestCase subclass: #OOStackTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:30'!
test01StackShouldBeEmptyWhenCreated

	| stack |
	
	stack := OOStack new.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:29:55'!
test02PushAddElementsToTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	
	self deny: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:01'!
test03PopRemovesElementsFromTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:09'!
test04PopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	stack push: pushedObject.
	
	self assert: stack pop = pushedObject! !

!OOStackTest methodsFor: 'test' stamp: 'NR 4/22/2021 17:25:26'!
test05StackBehavesLIFO

	| stack firstPushedObject secondPushedObject |
	
	stack := OOStack new.
	firstPushedObject := 'firstObject'.
	secondPushedObject := 'secondObject'.
	
	stack push: firstPushedObject.
	stack push: secondPushedObject.
	
	self assert: stack pop = secondPushedObject.
	self assert: stack pop = firstPushedObject.
	self assert: stack isEmpty 
	! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:20'!
test06TopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack top = pushedObject.
	! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:24'!
test07TopDoesNotRemoveObjectFromStack

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack size = 1.
	stack top.
	self assert: stack size = 1.
	! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:26'!
test08CanNotPopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:31'!
test09CanNotPopWhenThereAreNoObjectsInTheStackAndTheStackHadObjects

	| stack  |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:44'!
test10CanNotTopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack top ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !


!classDefinition: #SentenceFinderByPrefixTest category: 'Stack-Exercise'!
TestCase subclass: #SentenceFinderByPrefixTest
	instanceVariableNames: 'testStack testFinder emptyStack emptyStackFinder'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'LM 4/28/2021 12:23:59'!
test01EmptyPrefix

	| prefix |
	
	prefix := ''.
	self
		should: [ testFinder find: prefix ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = SentenceFinderByPrefix emptyPrefixErrorDescription ]! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'LM 4/28/2021 12:24:03'!
test02PrefixWithSpaces

	| prefix |
	
	prefix := 'Winter is'.
	self
		should: [ testFinder find: prefix ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = SentenceFinderByPrefix prefixHasSpacesErrorDescription ]! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'LM 4/27/2021 17:04:07'!
test03EmptyStack

	| prefix result |
	
	prefix := 'Wint'.
	result := OrderedCollection new.
	
	self assert: result equals: (emptyStackFinder find: prefix).! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'LM 4/27/2021 17:04:15'!
test04PrefixNotFound

	| prefix result |
	
	prefix := 'WIN'.
	result := OrderedCollection new.
	
	self assert: result equals: (testFinder find: prefix).! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'LM 4/27/2021 17:04:23'!
test05PrefixInOneSentence

	| prefix result |
	
	prefix := 'win'.
	result := OrderedCollection new.
	result addFirst: 'winning is everything'.
	
	self assert: result equals: (testFinder find: prefix).! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'LM 4/27/2021 17:04:29'!
test06PrefixInTwoSentences

	| prefix result |
	
	prefix := 'Wint'.
	result := OrderedCollection new.
	result		addLast: 'Winter is here';
			addLast: 'Winter is coming'.
	
	self assert: result equals: (testFinder find: prefix).! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'LM 4/27/2021 17:04:36'!
test07InputStackIsNotChangedWhenPrefixFound

	| prefix testStackCopy |
	
	prefix := 'winn'.
	testStackCopy := testStack veryDeepCopy.
	testFinder find: prefix.
	
	self assert: (self is: testStackCopy equalTo: testStack).! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'LM 4/27/2021 17:04:48'!
test08InputStackIsNotChangedWhenPrefixNotFound

	| prefix testStackCopy |
	
	prefix := 'coco'.
	testStackCopy := testStack veryDeepCopy.
	testFinder find: prefix.
	
	self assert: (self is: testStackCopy equalTo: testStack).! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'LM 4/27/2021 17:05:01'!
test09InputStackIsNotChangedWhenPrefixIsEmpty

	| prefix testStackCopy |
	
	prefix := ''.
	testStackCopy := testStack veryDeepCopy.
	
	self
		should: [ testFinder find: prefix ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: (self is: testStackCopy equalTo: testStack) ].! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'LM 4/27/2021 17:05:09'!
test10InputStackIsNotChangedWhenPrefixHasSpaces

	| prefix testStackCopy |
	
	prefix := 'winter is'.
	testStackCopy := testStack veryDeepCopy.
	
	self
		should: [ testFinder find: prefix ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: (self is: testStackCopy equalTo: testStack) ].! !


!SentenceFinderByPrefixTest methodsFor: 'misc' stamp: 'LM 4/26/2021 21:36:00'!
is: aStack equalTo: anotherStack

	aStack size = anotherStack size ifFalse: [^false].
	
	[aStack isEmpty] whileFalse: [
		aStack pop = anotherStack pop ifFalse: [^false]
	].
	
	^true! !


!SentenceFinderByPrefixTest methodsFor: 'setup' stamp: 'LM 4/28/2021 12:23:22'!
setUp

	testStack := OOStack new.
	testStack		push: 'Winter is coming';
			push: 'winning is everything';
			push: 'The winds of Winter';
			push: 'Winter is here'.
	emptyStack := OOStack new.
	
	testFinder := SentenceFinderByPrefix with: testStack.
	emptyStackFinder := SentenceFinderByPrefix with: emptyStack.! !


!classDefinition: #OOStack category: 'Stack-Exercise'!
Object subclass: #OOStack
	instanceVariableNames: 'elementAtTheTop'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStack methodsFor: 'initialization' stamp: 'LM 4/26/2021 22:19:25'!
initialize

	elementAtTheTop := StackBottom new.! !


!OOStack methodsFor: 'testing' stamp: 'LM 4/27/2021 10:03:20'!
isEmpty

	^self size = 0! !


!OOStack methodsFor: 'accessing' stamp: 'LM 4/27/2021 01:18:37'!
pop

	| top |
	
	top := self top.
	elementAtTheTop := elementAtTheTop next.
	
	^top! !

!OOStack methodsFor: 'accessing' stamp: 'LM 4/27/2021 01:33:08'!
size

	^elementAtTheTop numberOfElementsBelow! !

!OOStack methodsFor: 'accessing' stamp: 'LM 4/27/2021 01:18:45'!
top

	^elementAtTheTop value! !


!OOStack methodsFor: 'adding' stamp: 'LM 4/27/2021 01:40:53'!
push: topElement

	elementAtTheTop := StackTop with: topElement over: elementAtTheTop.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'OOStack class' category: 'Stack-Exercise'!
OOStack class
	instanceVariableNames: ''!

!OOStack class methodsFor: 'error descriptions' stamp: 'NR 4/22/2021 17:22:41'!
stackEmptyErrorDescription
	
	^ 'Stack is empty!!!!!!'! !


!classDefinition: #SentenceFinderByPrefix category: 'Stack-Exercise'!
Object subclass: #SentenceFinderByPrefix
	instanceVariableNames: 'backupStack stack'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefix methodsFor: 'initialization' stamp: 'LM 4/28/2021 17:44:59'!
initializeWith: aStack

	stack := aStack.	
	backupStack := OOStack new.! !


!SentenceFinderByPrefix methodsFor: 'finding' stamp: 'LM 4/28/2021 17:15:34'!
find: prefix

	| sentencesWithPrefix |
	
	self checkPrefix: prefix.
	
	sentencesWithPrefix := OrderedCollection new.
	self forSentencesWithPrefix: prefix do: [:sentence | sentencesWithPrefix addLast: sentence].

	^sentencesWithPrefix! !


!SentenceFinderByPrefix methodsFor: 'private' stamp: 'LM 4/28/2021 12:34:16'!
checkPrefix: prefix

	prefix isEmpty ifTrue: [self error: self class emptyPrefixErrorDescription].
	(prefix includesSubString: ' ') ifTrue: [self error: self class prefixHasSpacesErrorDescription].! !

!SentenceFinderByPrefix methodsFor: 'private' stamp: 'LM 4/28/2021 17:09:46'!
forSentencesWithPrefix: prefix do: aBlock

	| nextSentence |
	
	[stack isEmpty] whileFalse: [
		nextSentence := self nextSentence.
		(nextSentence beginsWith: prefix) ifTrue: [aBlock value: nextSentence].
	].
	
	self restoreStack.! !

!SentenceFinderByPrefix methodsFor: 'private' stamp: 'LM 4/28/2021 17:09:47'!
nextSentence

	backupStack push: stack pop.
	^backupStack top! !

!SentenceFinderByPrefix methodsFor: 'private' stamp: 'LM 4/28/2021 17:10:15'!
restoreStack

	[backupStack isEmpty] whileFalse: [stack push: backupStack pop].! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'SentenceFinderByPrefix class' category: 'Stack-Exercise'!
SentenceFinderByPrefix class
	instanceVariableNames: ''!

!SentenceFinderByPrefix class methodsFor: 'instance creation' stamp: 'LM 4/28/2021 12:26:58'!
with: aStack

	^self new initializeWith: aStack.! !


!SentenceFinderByPrefix class methodsFor: 'error descriptions' stamp: 'LM 4/28/2021 12:40:50'!
emptyPrefixErrorDescription

	^'Prefix cannot be empty!!!!!!'! !

!SentenceFinderByPrefix class methodsFor: 'error descriptions' stamp: 'LM 4/28/2021 12:41:24'!
prefixHasSpacesErrorDescription

	^'Prefix cannot have spaces!!!!!!'! !


!classDefinition: #StackElement category: 'Stack-Exercise'!
Object subclass: #StackElement
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!StackElement methodsFor: 'accessing' stamp: 'LM 4/27/2021 01:29:35'!
numberOfElementsBelow

	self subclassResponsibility.! !

!StackElement methodsFor: 'accessing' stamp: 'LM 4/26/2021 22:08:10'!
value

	self subclassResponsibility.! !


!classDefinition: #StackBottom category: 'Stack-Exercise'!
StackElement subclass: #StackBottom
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!StackBottom methodsFor: 'accessing' stamp: 'LM 4/27/2021 01:29:35'!
numberOfElementsBelow

	^0! !

!StackBottom methodsFor: 'accessing' stamp: 'LM 4/25/2021 21:48:52'!
value

	self error: OOStack stackEmptyErrorDescription.! !


!classDefinition: #StackTop category: 'Stack-Exercise'!
StackElement subclass: #StackTop
	instanceVariableNames: 'value next'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!StackTop methodsFor: 'accessing' stamp: 'LM 4/27/2021 01:23:34'!
next

	^next! !

!StackTop methodsFor: 'accessing' stamp: 'LM 4/27/2021 01:29:35'!
numberOfElementsBelow

	^1 + next numberOfElementsBelow! !

!StackTop methodsFor: 'accessing' stamp: 'LM 4/25/2021 21:49:10'!
value

	^value! !


!StackTop methodsFor: 'initialization' stamp: 'LM 4/27/2021 01:37:05'!
initializeWith: aValue over: aStackElement

	value := aValue.
	next := aStackElement.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'StackTop class' category: 'Stack-Exercise'!
StackTop class
	instanceVariableNames: ''!

!StackTop class methodsFor: 'as yet unclassified' stamp: 'LM 4/27/2021 01:36:14'!
with: aValue over: aStackElement

	^self new initializeWith: aValue over: aStackElement! !
