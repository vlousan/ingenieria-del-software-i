!classDefinition: #I category: 'Numeros naturales'!
DenotativeObject subclass: #I
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'I class' category: 'Numeros naturales'!
I class
	instanceVariableNames: ''!

!I class methodsFor: 'as yet unclassified' stamp: 'LM 4/6/2021 21:24:44'!
* unFactor

	^unFactor! !

!I class methodsFor: 'as yet unclassified' stamp: 'LM 4/6/2021 21:42:36'!
+ unSumando

	^unSumando next! !

!I class methodsFor: 'as yet unclassified' stamp: 'LM 4/6/2021 21:43:10'!
- unSustraendo

	^self error: self descripcionDeErrorDeNumerosNegativosNoSoportados! !

!I class methodsFor: 'as yet unclassified' stamp: 'LM 4/6/2021 21:55:36'!
/ unDivisor

	^self divideBy: unDivisor! !

!I class methodsFor: 'as yet unclassified' stamp: 'LM 4/7/2021 21:49:05'!
< unNumero

	^(self >= unNumero) not! !

!I class methodsFor: 'as yet unclassified' stamp: 'LM 4/7/2021 21:48:40'!
>= unNumero

	^self = unNumero! !

!I class methodsFor: 'as yet unclassified' stamp: 'LM 4/8/2021 10:36:44'!
descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor

	^'No se puede dividir por un n�mero mayor'! !

!I class methodsFor: 'as yet unclassified' stamp: 'LM 4/8/2021 10:36:37'!
descripcionDeErrorDeNumerosNegativosNoSoportados

	^'N�meros negativos no soportados'! !

!I class methodsFor: 'as yet unclassified' stamp: 'LM 4/6/2021 21:54:47'!
divideBy: unDivisor

	unDivisor = I ifTrue: [^I].
	^self error: self descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor! !

!I class methodsFor: 'as yet unclassified' stamp: 'LM 4/6/2021 22:04:29'!
next

	^II! !

!I class methodsFor: 'as yet unclassified' stamp: 'LM 4/6/2021 21:23:09'!
previous

	^self error: self descripcionDeErrorDeNumerosNegativosNoSoportados! !

!I class methodsFor: 'as yet unclassified' stamp: 'LM 4/6/2021 21:23:41'!
subtractFrom: unMinuendo

	^unMinuendo previous! !


!classDefinition: #II category: 'Numeros naturales'!
DenotativeObject subclass: #II
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'II class' category: 'Numeros naturales'!
II class
	instanceVariableNames: 'next previous'!

!II class methodsFor: 'as yet unclassified' stamp: 'LM 4/6/2021 21:25:05'!
* unFactor

	^unFactor + (self previous * unFactor)! !

!II class methodsFor: 'as yet unclassified' stamp: 'LM 4/6/2021 21:25:48'!
+ unSumando

	^self previous + unSumando next! !

!II class methodsFor: 'as yet unclassified' stamp: 'LM 4/6/2021 21:26:28'!
- unSustraendo

	^unSustraendo subtractFrom: self! !

!II class methodsFor: 'as yet unclassified' stamp: 'LM 4/6/2021 21:27:32'!
/ unDivisor

	self < unDivisor ifTrue: [^self error: self descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor].
	^self divideBy: unDivisor! !

!II class methodsFor: 'as yet unclassified' stamp: 'LM 4/7/2021 21:46:27'!
< unNumero

	^(self >= unNumero) not! !

!II class methodsFor: 'as yet unclassified' stamp: 'LM 4/7/2021 13:26:10'!
>= unNumero

	[unNumero - self] on: Error do: [^true].
	^false! !

!II class methodsFor: 'as yet unclassified' stamp: 'LM 4/8/2021 10:36:25'!
descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor

	^'No se puede dividir por un n�mero mayor'! !

!II class methodsFor: 'as yet unclassified' stamp: 'LM 4/7/2021 21:51:46'!
divideBy: unDivisor

	(self=unDivisor or: (self-unDivisor < unDivisor)) ifTrue: [^I].
	^I + ((self - unDivisor) divideBy: unDivisor)! !

!II class methodsFor: 'as yet unclassified' stamp: 'LM 4/8/2021 10:37:54'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := nil.
	previous := I.! !

!II class methodsFor: 'as yet unclassified' stamp: 'LM 4/6/2021 21:31:44'!
next

	next ifNil: [
		next _ self createChildNamed: self name , 'I'.
		next previous: self
	].
	^next! !

!II class methodsFor: 'as yet unclassified' stamp: 'LM 4/6/2021 21:31:38'!
previous

	^previous! !

!II class methodsFor: 'as yet unclassified' stamp: 'LM 4/6/2021 21:32:31'!
previous: unNumero

	previous _ unNumero.! !

!II class methodsFor: 'as yet unclassified' stamp: 'LM 4/6/2021 21:33:21'!
subtractFrom: unMinuendo

	^self previous subtractFrom: unMinuendo previous! !

II initializeAfterFileIn!