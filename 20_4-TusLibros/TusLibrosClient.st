!classDefinition: #TusLibrosClientHistoryWindow category: 'TusLibrosClient'!
Panel subclass: #TusLibrosClientHistoryWindow
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibrosClient'!

!TusLibrosClientHistoryWindow methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 16:54:53'!
buildMorphicWindow

	self titleMorph showButtonsNamed: #( close ).
	self morphExtent: (self defaultExtent).
	self layoutMorph
		addMorph: (PluggableListMorph model: self model listGetter: #historyList indexGetter: nil indexSetter: nil).
	
	self model when: #loggingOut send: #delete to: self.! !

!TusLibrosClientHistoryWindow methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 16:53:29'!
defaultExtent

	^330@280! !


!classDefinition: #TusLibrosClientLoginWindow category: 'TusLibrosClient'!
Panel subclass: #TusLibrosClientLoginWindow
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibrosClient'!

!TusLibrosClientLoginWindow methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 09:19:04'!
buildLoginLayout

	| createCartButton passwordEntry userEntry |
	
	userEntry := OneLineEditorMorph contents: ''.
	userEntry setProperty: #keyStroke: toValue: [ :key | self model clientID: userEntry contents ].
	userEntry borderWidth: 1; borderColor: Color skyBlue; morphWidth: 300.
	passwordEntry := OneLineEditorMorph contents: ''.
	passwordEntry setProperty: #keyStroke: toValue: [ :key | self model password: passwordEntry contents ].
	passwordEntry borderWidth: 1; borderColor: Color skyBlue; morphWidth: 300.
	
	createCartButton := PluggableButtonMorph model: self model stateGetter: nil action: #createCart  label: 'Create cart'.
	userEntry crAction: [ createCartButton performAction ].
	passwordEntry crAction: [ createCartButton performAction ].
	
	self layoutMorph
	beColumn;
	separation: 15;
	axisEdgeWeight: 0;
	addMorph: (LabelMorph contents:'Client ID');
	addMorph: userEntry;
	addMorph: (LabelMorph contents:'Password');
	addMorph: passwordEntry;
 	addMorph: createCartButton.! !

!TusLibrosClientLoginWindow methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 09:18:15'!
buildMorphicWindow
		
	self titleMorph showButtonsNamed: #( close collapse ).
	self morphExtent: (self defaultExtent).
	self buildLoginLayout.
	
	self model when: #cartCreated send: #proceedToShopping to: self.! !

!TusLibrosClientLoginWindow methodsFor: 'as yet unclassified' stamp: 'LM 7/1/2021 11:17:57'!
defaultExtent

	^330@280! !

!TusLibrosClientLoginWindow methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 10:27:09'!
proceedToShopping

	TusLibrosClientShopWindow open: self model label: 'TusLibros Shop'.
	self delete.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TusLibrosClientLoginWindow class' category: 'TusLibrosClient'!
TusLibrosClientLoginWindow class
	instanceVariableNames: ''!

!TusLibrosClientLoginWindow class methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 09:17:40'!
open
	
	^self open: TusLibrosClientWindowModel new label: 'TusLibros Client Window'.! !


!classDefinition: #TusLibrosClientShopWindow category: 'TusLibrosClient'!
Panel subclass: #TusLibrosClientShopWindow
	instanceVariableNames: 'totalLabel cartListMorph'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibrosClient'!

!TusLibrosClientShopWindow methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 12:53:12'!
buildCartLayout

	| cartLabel layout buttonsLayout |
	
	cartLabel := LabelMorph contents: 'Cart'.
	cartListMorph := PluggableListMorph model: self model listGetter: #cartList indexGetter: #cartListIndex indexSetter: #cartListIndex:.
	cartListMorph layoutSpec proportionalWidth: 0.85; proportionalHeight: 0.8.
	totalLabel := LabelMorph contents: 'Total: $0'.
	buttonsLayout := LayoutMorph newRow
		addMorph: (PluggableButtonMorph model: self model action: #listPurchases label: 'History');
		addMorph: (PluggableButtonMorph model: self model action: #checkOutCart label: 'Check out');
		yourself.
	
	layout := LayoutMorph newColumn.
	
	layout layoutSpec proportionalWidth: 0.4; proportionalHeight: 1.0.
	
	^layout separation: 5; 
	axisEdgeWeight: 0.0;
	addMorph: cartLabel;
	addMorph: cartListMorph;
	addMorph: totalLabel;
	addMorph: buttonsLayout;
	yourself! !

!TusLibrosClientShopWindow methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 11:25:03'!
buildCatalogLayout

	| catalogLabel catalogList layout |
	
	catalogLabel := LabelMorph contents: 'Catalog'.
	catalogList := PluggableListMorph model: self model listGetter: #catalogList indexGetter: #catalogListIndex indexSetter: #catalogListIndex:.
	catalogList layoutSpec proportionalWidth: 0.85; proportionalHeight: 0.95.
	
	layout := LayoutMorph newColumn.
	
	layout layoutSpec proportionalWidth: 0.4; proportionalHeight: 1.0.
	
	^layout separation: 5; 
	axisEdgeWeight: 0.0;
	addMorph: catalogLabel;
	addMorph: catalogList;
	yourself! !

!TusLibrosClientShopWindow methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 13:31:13'!
buildControlsLayout

	| layout buttonsLayout quantityEntry quantityLabel |
	
	quantityLabel := LabelMorph contents: 'Quantity'.
	quantityEntry := SimpleNumberEntryMorph integerDefault: 1 maxNumChars: 10.
	quantityEntry valueUpdator: [ :value | self model quantity: value ].
	buttonsLayout := LayoutMorph newRow
		addMorph: (PluggableButtonMorph model: self model action: #removeEnteredQuantityOfSelectedItem label: '-');
		addMorph: (PluggableButtonMorph model: self model action: #addEnteredQuantityOfSelectedItem label: '+').
	
	layout := LayoutMorph newColumn.
	
	layout layoutSpec proportionalWidth: 0.2; proportionalHeight: 1.0.
	
	^layout
	addMorph: quantityLabel;
	addMorph: quantityEntry;
	addMorph: buttonsLayout.! !

!TusLibrosClientShopWindow methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 18:31:39'!
buildMorphicWindow

	self titleMorph showButtonsNamed: #( close collapse ).
	self morphExtent: (self defaultExtent).
	
	self layoutMorph beRow;
	addMorph: self buildCatalogLayout;
	addMorph: self buildControlsLayout;
	addMorph: self buildCartLayout.
	
	self model when: #cartUpdated send: #updateCartListAndTotal to: self.
	self model when: #successfulCheckOut send: #showTicket to: self.
	self model when: #failedCheckOut send: #showCheckOutError to: self.
	self model when: #historyArrived send: #openHistory to: self.
	self model when: #loggingOut send: #delete to: self.! !

!TusLibrosClientShopWindow methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 09:20:50'!
defaultExtent

	^810@570! !

!TusLibrosClientShopWindow methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 16:54:30'!
openHistory

	TusLibrosClientHistoryWindow open: self model label: 'Purchases history'.! !

!TusLibrosClientShopWindow methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 16:58:32'!
showCheckOutError

	TusLibrosClientCheckOutErrorWindow open: self model label: 'Error'.! !

!TusLibrosClientShopWindow methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 16:28:58'!
showTicket

	TusLibrosClientTicketWindow open: self model label: 'Ticket'.! !

!TusLibrosClientShopWindow methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 13:13:28'!
updateCartListAndTotal

	cartListMorph updateList.
	totalLabel contents: 'Total: $', self model cartTotal asString.! !


!classDefinition: #TusLibrosClientTicketWindow category: 'TusLibrosClient'!
Panel subclass: #TusLibrosClientTicketWindow
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibrosClient'!

!TusLibrosClientTicketWindow methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 16:38:41'!
buildMorphicWindow

	self titleMorph showButtonsNamed: #( close ).
	self morphExtent: (self defaultExtent).
	self layoutMorph
		addMorph: (PluggableListMorph model: self model listGetter: #ticketList indexGetter: nil indexSetter: nil);
		addMorph: (LayoutMorph newRow
								addMorph: (PluggableButtonMorph model: self model action: #logOut label: 'Quit');
								addMorph: (PluggableButtonMorph model: self action: #delete label: 'Keep buying');
								yourself).
	
	self model when: #loggingOut send: #delete to: self.! !

!TusLibrosClientTicketWindow methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 16:28:38'!
defaultExtent

	^330@280! !


!classDefinition: #TusLibrosClientCheckOutErrorWindow category: 'TusLibrosClient'!
Object subclass: #TusLibrosClientCheckOutErrorWindow
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibrosClient'!

!TusLibrosClientCheckOutErrorWindow methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 17:00:07'!
buildMorphicWindow

	self titleMorph showButtonsNamed: #( close ).
	self morphExtent: (self defaultExtent).
	self layoutMorph
		addMorph: (LabelMorph contents: 'An error occurred during check out').
	
	self model when: #loggingOut send: #delete to: self.! !

!TusLibrosClientCheckOutErrorWindow methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 17:00:32'!
defaultExtent

	^500@280! !


!classDefinition: #TusLibrosClientRestInterface category: 'TusLibrosClient'!
Object subclass: #TusLibrosClientRestInterface
	instanceVariableNames: 'port'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibrosClient'!

!TusLibrosClientRestInterface methodsFor: 'initialization' stamp: 'LM 6/28/2021 19:53:20'!
initializeSendingThroughPort: aPortNumber

	port := aPortNumber.! !


!TusLibrosClientRestInterface methodsFor: 'as yet unclassified' stamp: 'LM 6/28/2021 19:50:24'!
port
	
	^port ifNil: [port:=8080].! !

!TusLibrosClientRestInterface methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 12:16:13'!
sendAddToCartRequestWithCartId: aCartID quantity: aQuantity item: anISBN

	^self sendRequestForService: '/addToCart' withArguments: {'cartId'.aCartID . 'bookIsbn'.anISBN . 'bookQuantity'.aQuantity} withMethod: 'POST'! !

!TusLibrosClientRestInterface methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 15:37:33'!
sendCheckOutCartRequestWithCartID: aCartID

	^self sendRequestForService: '/checkOutCart' withArguments: {'cartId'. aCartID} withMethod: 'POST'! !

!TusLibrosClientRestInterface methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 12:16:13'!
sendCreateCartRequestWithClientID: aClientID password: aPassword

	^self sendRequestForService: '/createCart' withArguments: {'clientId'. aClientID. 'password'. aPassword} withMethod: 'POST'! !

!TusLibrosClientRestInterface methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 12:16:22'!
sendGetCatalogRequest

	^self sendRequestForService: '/getCatalog' withArguments: {} withMethod: 'GET'! !

!TusLibrosClientRestInterface methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 18:16:57'!
sendListPurchasesRequestWithClientID: aClientID password: aPassword

	^self sendRequestForService: '/listPurchases' withArguments: {'clientId'. aClientID. 'password'. aPassword} withMethod: 'GET'! !

!TusLibrosClientRestInterface methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 13:32:54'!
sendRemoveFromCartRequestWithCartId: aCartID quantity: aQuantity item: anISBN

	^self sendRequestForService: '/removeFromCart' withArguments: {'cartId'.aCartID . 'bookIsbn'.anISBN . 'bookQuantity'.aQuantity} withMethod: 'POST'! !

!TusLibrosClientRestInterface methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 12:16:13'!
sendRequestForService: aServiceString withArguments: anArgumentsArray withMethod: anHttpMethod 

	| fieldDict resp |
	
	fieldDict := Dictionary newFromPairs: anArgumentsArray.
	
	resp := WebClient htmlSubmit: (self url, aServiceString) fields: fieldDict.
	
	resp isSuccess 
		ifTrue:[^WebUtils jsonDecode: (resp content) readStream.] 
		ifFalse:[^self error: resp content].! !

!TusLibrosClientRestInterface methodsFor: 'as yet unclassified' stamp: 'LM 6/28/2021 19:50:03'!
url
	
	^'http://localhost:', self port asString! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TusLibrosClientRestInterface class' category: 'TusLibrosClient'!
TusLibrosClientRestInterface class
	instanceVariableNames: ''!

!TusLibrosClientRestInterface class methodsFor: 'instance creation' stamp: 'LM 6/28/2021 19:52:44'!
sendingThroughPort: aPortNumber

	^self new initializeSendingThroughPort: aPortNumber! !


!classDefinition: #TusLibrosClientWindowModel category: 'TusLibrosClient'!
Object subclass: #TusLibrosClientWindowModel
	instanceVariableNames: 'clientID password restInterface cartID catalog catalogListIndex quantity cartContents cartListIndex lastTicket history'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibrosClient'!

!TusLibrosClientWindowModel methodsFor: 'initialization' stamp: 'LM 7/8/2021 13:35:07'!
initialize

	clientID := ''.
	password := ''.
	restInterface := TusLibrosClientRestInterface new.
	catalogListIndex := 1.
	cartListIndex := 1.
	quantity := 1.
	cartContents := Dictionary new.! !


!TusLibrosClientWindowModel methodsFor: 'accessing' stamp: 'LM 6/28/2021 21:05:10'!
clientID

	^clientID! !

!TusLibrosClientWindowModel methodsFor: 'accessing' stamp: 'LM 6/28/2021 21:25:06'!
clientID: aClientID

	clientID _ aClientID! !

!TusLibrosClientWindowModel methodsFor: 'accessing' stamp: 'LM 6/28/2021 21:06:56'!
password

	^password! !

!TusLibrosClientWindowModel methodsFor: 'accessing' stamp: 'LM 6/28/2021 21:25:41'!
password: aPassword

	password _ aPassword! !

!TusLibrosClientWindowModel methodsFor: 'accessing' stamp: 'LM 7/8/2021 11:48:14'!
quantity: aQuantity

	quantity := aQuantity.! !


!TusLibrosClientWindowModel methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 13:38:04'!
addEnteredQuantityOfSelectedItem

	catalogListIndex < 1 ifTrue: [ ^self ].
	
	cartContents := restInterface sendAddToCartRequestWithCartId: cartID quantity: quantity item: (catalog keys at: catalogListIndex).

	self triggerEvent: #cartUpdated with: self.! !

!TusLibrosClientWindowModel methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 12:32:23'!
cartList

	| cartList |
	
	cartList := OrderedCollection new.
	cartContents keysAndValuesDo: [ :isbn :aQuantity |
		cartList add: aQuantity asString, ' x ', (self listItemStringFor: isbn).
	].

	^cartList

	! !

!TusLibrosClientWindowModel methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 12:47:13'!
cartListIndex

	^cartListIndex! !

!TusLibrosClientWindowModel methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 12:47:28'!
cartListIndex: anIndex

	cartListIndex := anIndex.! !

!TusLibrosClientWindowModel methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 13:17:41'!
cartTotal

	| cartTotal |
	
	cartTotal := 0.
	cartContents keysAndValuesDo: [ :isbn :aQuantity |
		cartTotal := cartTotal + (aQuantity * (catalog at: isbn) second).
	].

	^cartTotal
	! !

!TusLibrosClientWindowModel methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 12:30:12'!
catalogList

	| catalogList |
	
	catalogList := OrderedCollection new.
	catalog keysDo: [ :isbn | catalogList add: (self listItemStringFor: isbn) ].
	
	^catalogList! !

!TusLibrosClientWindowModel methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 10:25:11'!
catalogListIndex

	^catalogListIndex! !

!TusLibrosClientWindowModel methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 10:25:35'!
catalogListIndex: anIndex

	catalogListIndex := anIndex.! !

!TusLibrosClientWindowModel methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 16:50:28'!
checkOutCart

	[ lastTicket := restInterface sendCheckOutCartRequestWithCartID: cartID ]
		on: Error
		do: [ ^self triggerEvent: #failedCheckout with: self ].
	
	self triggerEvent: #successfulCheckOut with: self.! !

!TusLibrosClientWindowModel methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 10:10:56'!
createCart

	cartID := restInterface sendCreateCartRequestWithClientID: clientID password: password.
	catalog := restInterface sendGetCatalogRequest.
	
	self triggerEvent: #cartCreated with: self.! !

!TusLibrosClientWindowModel methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 18:34:13'!
historyList

	| historyList |
	
	historyList := OrderedCollection new.
	history first keysAndValuesDo: [ :isbn :aQuantity |
		historyList add: aQuantity asString, ' x ', (self listItemStringFor: isbn).
	].
	historyList add: 'Total: $', history second asString.

	^historyList! !

!TusLibrosClientWindowModel methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 12:29:30'!
listItemStringFor: anISBN

	^'[', anISBN, '] ', (catalog at: anISBN) first, ' - $', (catalog at: anISBN) second asString! !

!TusLibrosClientWindowModel methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 18:32:37'!
listPurchases

	history := restInterface sendListPurchasesRequestWithClientID: clientID password: password.
	
	self triggerEvent: #historyArrived with: self.! !

!TusLibrosClientWindowModel methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 16:37:35'!
logOut

	self triggerEvent: #loggingOut with: self.! !

!TusLibrosClientWindowModel methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 16:45:28'!
removeEnteredQuantityOfSelectedItem

	cartListIndex < 1 ifTrue: [ ^self ].
	
	cartContents := restInterface sendRemoveFromCartRequestWithCartId: cartID quantity: quantity item: (cartContents keys at: cartListIndex).

	self triggerEvent: #cartUpdated with: self.! !

!TusLibrosClientWindowModel methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 16:29:56'!
ticketList

	| ticketList total |
	ticketList := OrderedCollection new.
	total := lastTicket second.
	
	lastTicket first do: [ :lineItem |
		ticketList add: lineItem second asString, 'x [', lineItem first, '] ', (catalog at: lineItem first) first, ' - $', lineItem third asString.
	].
	ticketList add: 'Total: $', total asString.
	
	^ticketList! !
