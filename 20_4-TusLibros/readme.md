Para iniciar el servidor, correr en un workspace:

```
test := TusLibrosSystemFacadeTest new setUp; yourself.
factory := StoreTestObjectsFactory new.
salesBook := OrderedCollection new.
facade := TusLibrosSystemFacade authenticatingWith: test validUsersAndPasswords withRegisteredCreditCards: test usersCreditCards acceptingItemsOf: factory defaultCatalog registeringOn: salesBook debitingThrought: test measuringTimeWith: DateAndTime.
server := TusLibrosStoreRestInterface listeningOn: 8080 delegatingTo: facade.
```

Para abrir el cliente, correr:

```
TusLibrosClientLoginWindow open.
```

Ingresar con las credenciales `validUserWithRegisteredCreditCard` y `validUserPassword`.