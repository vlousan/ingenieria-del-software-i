!classDefinition: #TusLibrosStoreRestInterface category: 'TusLibrosStoreServer'!
Object subclass: #TusLibrosStoreRestInterface
	instanceVariableNames: 'port systemFacade webServer'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibrosStoreServer'!

!TusLibrosStoreRestInterface methodsFor: 'initialization' stamp: 'LM 7/8/2021 09:52:27'!
initializeListeningOn: aPortNumber delegatingTo: aSystemFacade

	port := aPortNumber.
	systemFacade := aSystemFacade.
	
	webServer := WebServer new listenOn: self port.
	
	webServer addService: '/createCart' action: (self getResponseActionFor: [ :request | self processCreateCartRequest: request]).
	webServer addService: '/addToCart' action: (self getResponseActionFor: [ :request | self processAddToCartRequest: request]).
	webServer addService: '/removeFromCart' action: (self getResponseActionFor: [ :request | self processRemoveFromCartRequest: request]).
	webServer addService: '/listCart' action: (self getResponseActionFor: [ :request | self processListCartRequest: request]).
	webServer addService: '/checkOutCart' action: (self getResponseActionFor: [ :request | self processCheckOutCartRequest: request]).
	webServer addService: '/listPurchases' action: (self getResponseActionFor: [ :request | self processListPurchasesRequest: request]).
	webServer addService: '/getCatalog' action: (self getResponseActionFor: [ :request | self processGetCatalogRequest: request]).! !


!TusLibrosStoreRestInterface methodsFor: 'as yet unclassified' stamp: 'LM 6/28/2021 12:17:08'!
destroy

	webServer destroy.! !

!TusLibrosStoreRestInterface methodsFor: 'as yet unclassified' stamp: 'LM 6/28/2021 10:56:26'!
getResponseActionFor: aRequestProcessingBlock

	^[ :request |
		| resultAsJson result |
		
		[
			result := aRequestProcessingBlock value: request.
			resultAsJson:= WebUtils jsonEncode: (result).
			request send200Response: (resultAsJson)
		] 
		on: Error 
		do: [ :anError |
			request send400Response: (anError messageText) ]
	]! !

!TusLibrosStoreRestInterface methodsFor: 'as yet unclassified' stamp: 'LM 6/28/2021 10:56:58'!
port

	^port! !

!TusLibrosStoreRestInterface methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 13:07:56'!
processAddToCartRequest: aRequest

	| cartID bookIsbn bookQuantity |
	
	cartID := (aRequest fields at: 'cartId') asNumber.
	bookIsbn := aRequest fields at: 'bookIsbn'.
	bookQuantity := (aRequest fields at: 'bookQuantity') asNumber.
	systemFacade add: bookQuantity of: bookIsbn toCartIdentifiedAs: cartID.
	^(systemFacade listCartIdentifiedAs: cartID) contents! !

!TusLibrosStoreRestInterface methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 15:34:24'!
processCheckOutCartRequest: aRequest

	| cartID cced ccn cco ticket |
	
	cartID := (aRequest fields at: 'cartId') asNumber.
	aRequest fields size = 1
		ifTrue: [ ticket := systemFacade checkOutCartIdentifiedAs: cartID ]
		ifFalse: [	
			ccn := (aRequest fields at: 'ccn') asNumber.
			cced := (aRequest fields at: 'cced') asMonth.
			cco := aRequest fields at: 'cco'.
			ticket := systemFacade checkOutCartIdentifiedAs: cartID withCreditCardNumbered: ccn ownedBy: cco expiringOn: cced.
		].
	^ticket! !

!TusLibrosStoreRestInterface methodsFor: 'as yet unclassified' stamp: 'LM 6/28/2021 10:40:11'!
processCreateCartRequest: aRequest

	| clientID password |
	
	clientID := aRequest fields at: 'clientId'.
	password := aRequest fields at: 'password'.
	^systemFacade createCartFor: clientID authenticatedWith: password! !

!TusLibrosStoreRestInterface methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 09:54:08'!
processGetCatalogRequest: aRequest

	^systemFacade catalog! !

!TusLibrosStoreRestInterface methodsFor: 'as yet unclassified' stamp: 'LM 6/28/2021 11:59:39'!
processListCartRequest: aRequest

	| cartID |
	
	cartID := (aRequest fields at: 'cartId') asNumber.
	^(systemFacade listCartIdentifiedAs: cartID) contents! !

!TusLibrosStoreRestInterface methodsFor: 'as yet unclassified' stamp: 'LM 7/1/2021 12:55:24'!
processListPurchasesRequest: aRequest

	| clientID password |
	
	clientID := aRequest fields at: 'clientId'.
	password := aRequest fields at: 'password'.
	^(systemFacade listPurchasesOf: clientID authenticatingWith: password) asArray! !

!TusLibrosStoreRestInterface methodsFor: 'as yet unclassified' stamp: 'LM 7/8/2021 13:09:25'!
processRemoveFromCartRequest: aRequest

	| cartID bookIsbn bookQuantity |
	
	cartID := (aRequest fields at: 'cartId') asNumber.
	bookIsbn := aRequest fields at: 'bookIsbn'.
	bookQuantity := (aRequest fields at: 'bookQuantity') asNumber.
	systemFacade remove: bookQuantity of: bookIsbn fromCartIdentifiedAs: cartID.
	^(systemFacade listCartIdentifiedAs: cartID) contents! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TusLibrosStoreRestInterface class' category: 'TusLibrosStoreServer'!
TusLibrosStoreRestInterface class
	instanceVariableNames: ''!

!TusLibrosStoreRestInterface class methodsFor: 'instance creation' stamp: 'LM 6/28/2021 10:28:06'!
listeningOn: aPortNumber delegatingTo: aSystemFacade 

	^self new initializeListeningOn: aPortNumber delegatingTo: aSystemFacade! !
